﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="DialogBox" Type="Folder">
			<Item Name="OneButtonDialogBox.lvlib" Type="Library" URL="../source/OneButtonDialogBox/OneButtonDialogBox.lvlib"/>
			<Item Name="TwoButtonDialogBox.lvlib" Type="Library" URL="../source/TwoButtonDialobBox/TwoButtonDialogBox.lvlib"/>
		</Item>
		<Item Name="externals" Type="Folder">
			<Item Name="composed-af-messages" Type="Folder">
				<Item Name="MessageCycle.lvclass" Type="LVClass" URL="../externals/composed-af-messages/Source/MessageCycle/MessageCycle.lvclass"/>
				<Item Name="PriorityStopMessage.lvclass" Type="LVClass" URL="../externals/composed-af-messages/Source/PriorityStopMessage/PriorityStopMessage.lvclass"/>
				<Item Name="ReturnMessageAdapter.lvclass" Type="LVClass" URL="../externals/composed-af-messages/Source/ReturnMessageAdapter/ReturnMessageAdapter.lvclass"/>
				<Item Name="RoundTripMessage.lvclass" Type="LVClass" URL="../externals/composed-af-messages/Source/RoundTripMessage/RoundTripMessage.lvclass"/>
			</Item>
			<Item Name="composed-event-logger" Type="Folder">
				<Item Name="Filters" Type="Folder">
					<Item Name="Compound Filter" Type="Folder">
						<Item Name="Compound Filter.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Filters/Compound Filter/Compound Filter.lvclass"/>
					</Item>
					<Item Name="Event Keyword Filter" Type="Folder">
						<Item Name="Event Keyword Filter.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Filters/Event Keyword Filter/Event Keyword Filter.lvclass"/>
					</Item>
					<Item Name="Event Level Filter" Type="Folder">
						<Item Name="Event Level Filter.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Filters/Event Level Filter/Event Level Filter.lvclass"/>
					</Item>
					<Item Name="Event Source Filter" Type="Folder">
						<Item Name="Event Source Filter.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Filters/Event Source Filter/Event Source Filter.lvclass"/>
					</Item>
				</Item>
				<Item Name="String Expression Format" Type="Folder">
					<Item Name="String Expression Format.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/String Formats/String Expression Format/String Expression Format.lvclass"/>
				</Item>
				<Item Name="Text File" Type="Folder">
					<Item Name="Text File.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Text File Sink/Text File/Text File.lvclass"/>
				</Item>
				<Item Name="Buffered Log Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Buffered Log Sink/Buffered Log Sink.lvclass"/>
				<Item Name="Buffered String Log Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Buffered String Log Sink/Buffered String Log Sink.lvclass"/>
				<Item Name="Composed Log.lvlib" Type="Library" URL="../externals/composed-event-logger/Source/Composed Log/Composed Log.lvlib"/>
				<Item Name="ConsoleView.lvlib" Type="Library" URL="../externals/composed-event-logger/Source/ConsoleView/ConsoleView.lvlib"/>
				<Item Name="IStringFormat.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/IStringFormat/IStringFormat.lvclass"/>
				<Item Name="LVQueue Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/LVQueue Sink/LVQueue Sink.lvclass"/>
				<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
				<Item Name="String Log Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/String Log Sink/String Log Sink.lvclass"/>
				<Item Name="SystemLog Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/SystemLog Sink/SystemLog Sink.lvclass"/>
				<Item Name="Text File Sink.lvclass" Type="LVClass" URL="../externals/composed-event-logger/Source/Text File Sink/Text File Sink.lvclass"/>
			</Item>
			<Item Name="composed-transport" Type="Folder">
				<Item Name="ActorMessageTransport.lvlib" Type="Library" URL="../externals/composed-transport/Source/Concrete/ActorMessageTransport/ActorMessageTransport.lvlib"/>
				<Item Name="BooleanTextBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/BooleanTextBinding/BooleanTextBinding.lvclass"/>
				<Item Name="ConfigurationFileBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/ConfigurationFileBinding/ConfigurationFileBinding.lvclass"/>
				<Item Name="EventTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/EventTransport/EventTransport.lvclass"/>
				<Item Name="FormattedStringBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/FormattedStringBinding/FormattedStringBinding.lvclass"/>
				<Item Name="ITransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/ITransport/ITransport.lvclass"/>
				<Item Name="ListboxItemNamesBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/ListboxItemNamesBinding/ListboxItemNamesBinding.lvclass"/>
				<Item Name="MCListboxItemNamesBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/MCListboxItemNamesBinding/MCListboxItemNamesBinding.lvclass"/>
				<Item Name="NotifierTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/NotifierTransport/NotifierTransport.lvclass"/>
				<Item Name="PersistToDiskTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/PersistToDiskTransport/PersistToDiskTransport.lvclass"/>
				<Item Name="QueueTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/QueueTransport/QueueTransport.lvclass"/>
				<Item Name="RingStringsBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/RingStringsBinding/RingStringsBinding.lvclass"/>
				<Item Name="StreamToDiskTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/StreamToDiskTransport/StreamToDiskTransport.lvclass"/>
				<Item Name="TerminalCaptionBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalCaptionBinding/TerminalCaptionBinding.lvclass"/>
				<Item Name="TerminalEnabledStateBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalEnabledStateBinding/TerminalEnabledStateBinding.lvclass"/>
				<Item Name="TerminalPropertyBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalPropertyBinding/TerminalPropertyBinding.lvclass"/>
				<Item Name="TerminalSignalingValueBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalSignalingValueBinding/TerminalSignalingValueBinding.lvclass"/>
				<Item Name="TerminalValueBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalValueBinding/TerminalValueBinding.lvclass"/>
				<Item Name="TerminalVisibilityBinding.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/TerminalVisibilityBinding/TerminalVisibilityBinding.lvclass"/>
				<Item Name="ValueReferenceTransport.lvclass" Type="LVClass" URL="../externals/composed-transport/Source/Concrete/ValueReferenceTransport/ValueReferenceTransport.lvclass"/>
			</Item>
			<Item Name="mva-core" Type="Folder">
				<Item Name="IMediator.lvlib" Type="Library" URL="../externals/mva-core/Source/IMediator/IMediator.lvlib"/>
				<Item Name="IModel.lvlib" Type="Library" URL="../externals/mva-core/Source/IModel/IModel.lvlib"/>
				<Item Name="IObserver.lvlib" Type="Library" URL="../externals/mva-core/Source/IObserver/IObserver.lvlib"/>
				<Item Name="Mediator.lvlib" Type="Library" URL="../externals/mva-core/Source/Mediator/Mediator.lvlib"/>
				<Item Name="MonitoredMediator.lvlib" Type="Library" URL="../externals/mva-core/Source/MonitoredMediator/MonitoredMediator.lvlib"/>
				<Item Name="PublicationPolicy.lvlib" Type="Library" URL="../externals/mva-core/Source/PublicationPolicy/PublicationPolicy.lvlib"/>
				<Item Name="RootModel.lvlib" Type="Library" URL="../externals/mva-core/Source/RootModel/RootModel.lvlib"/>
				<Item Name="SubscriptionPolicy.lvlib" Type="Library" URL="../externals/mva-core/Source/SubscriptionPolicy/SubscriptionPolicy.lvlib"/>
			</Item>
			<Item Name="mva-viewable" Type="Folder">
				<Item Name="ActorEvents.lvlib" Type="Library" URL="../externals/mva-viewable/Source/ActorEvents/ActorEvents.lvlib"/>
				<Item Name="IViewable.lvlib" Type="Library" URL="../externals/mva-viewable/Source/IViewable/IViewable.lvlib"/>
				<Item Name="IViewManager.lvlib" Type="Library" URL="../externals/mva-viewable/Source/IViewManager/IViewManager.lvlib"/>
				<Item Name="IViewModel.lvlib" Type="Library" URL="../externals/mva-viewable/Source/IViewModel/IViewModel.lvlib"/>
			</Item>
			<Item Name="variant-extensions" Type="Folder">
				<Item Name="VariantExtensions.lvlib" Type="Library" URL="../externals/variant-extensions/Source/VariantExtensions.lvlib"/>
			</Item>
		</Item>
		<Item Name="DialogBoxViewManager.lvlib" Type="Library" URL="../source/DialogBoxViewManager/DialogBoxViewManager.lvlib"/>
		<Item Name="IDialogBox.lvlib" Type="Library" URL="../source/IDialogBox/IDialogBox.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
